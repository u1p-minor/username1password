<?xml version="1.0" encoding="utf-8"?>

<html xmlns="http://www.w3.org/1999/xhtml"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xsl:version="1.0">
  <head>


      <!-- [title] -->
    <title>
      <xsl:value-of select="TEI/teiHeader/fileDesc/titleStmt/title" />
    </title>







































      <!-- [links] -->
    <link>
      <xsl:attribute name="href">
        <xsl:value-of select="TEI/teiHeader/xenoData/ref[1]/@target" />
      </xsl:attribute>
      <xsl:attribute name="rel">
        <xsl:value-of select="TEI/teiHeader/xenoData/ref[1]/desc" />
      </xsl:attribute>
    </link>
    <link>
      <xsl:attribute name="href">
        <xsl:value-of select="TEI/teiHeader/xenoData/ref[2]/@target" />
      </xsl:attribute>
      <xsl:attribute name="rel">
        <xsl:value-of select="TEI/teiHeader/xenoData/ref[2]/desc" />
      </xsl:attribute>
    </link>

  </head>
  <body>
      <!-- pre-main HTML5 [header or XHTML div] -->
    <div>
        <!-- [centering div] -->
      <div>
        <xsl:attribute name="class">
          <xsl:value-of select="TEI/text/front/figure/@style" />
        </xsl:attribute>
          <!-- header [img] -->
        <img>
            <!-- [alt] attribute -->
          <xsl:attribute name="alt">
            <xsl:value-of select="TEI/text/front/figure/graphic/desc" />
          </xsl:attribute>
          <xsl:attribute name="class">
            <xsl:value-of select="TEI/text/front/figure/graphic/@width" />
          </xsl:attribute>
          <xsl:attribute name="src">
            <xsl:value-of select="TEI/text/front/figure/graphic/@url" />
          </xsl:attribute>
        </img>
      </div>
        <!-- [h1] heading -->
      <h1>
        <xsl:attribute name="class">
          <xsl:value-of select="TEI/text/front/title/@style" />
        </xsl:attribute>
        <xsl:value-of select="TEI/text/front/title" />
      </h1>
    </div>
      <!-- [hr 72.1%] -->
    <hr>
      <xsl:attribute name="class">
        <xsl:value-of select="TEI/text/figure[1]/milestone/@style" />
      </xsl:attribute>
    </hr>
      <!-- HTML5 [main or XHTML div] -->
    <div>
        <!-- pre-articular HTML5 [header or XHTML div] -->
      <div>
          <!-- any number of text [p]aragraphs or similar divisions -->
        <p>
          <xsl:attribute name="class">
            <xsl:value-of select="TEI/text/body/head/p[1]/@style" />
          </xsl:attribute>
          <xsl:value-of select="TEI/text/body/head/p[1]/s[1]" />


          <xsl:value-of select="TEI/text/body/head/p[1]/s[2]" />


          <xsl:value-of select="TEI/text/body/head/p[1]/s[3]" />

        </p>
        <p>
          <xsl:attribute name="class">
            <xsl:value-of select="TEI/text/body/head/p[2]/@style" />
          </xsl:attribute>
          <xsl:value-of select="TEI/text/body/head/p[2]/s[1]" />


          <xsl:value-of select="TEI/text/body/head/p[2]/s[2]" />


          <xsl:value-of select="TEI/text/body/head/p[2]/s[3]" />

        </p>
        <p>
          <xsl:attribute name="class">
            <xsl:value-of select="TEI/text/body/head/p[3]/@style" />
          </xsl:attribute>
          <xsl:value-of select="TEI/text/body/head/p[3]/s[1]/seg[1]" />
          <a>
            <xsl:attribute name="href">
              <xsl:value-of select="TEI/text/body/head/p[3]/s[1]/ref[1]/@target" />
            </xsl:attribute>
            <xsl:value-of select="TEI/text/body/head/p[3]/s[1]/ref[1]/desc" /></a>




          <xsl:value-of select="TEI/text/body/head/p[3]/s[1]/seg[2]" />
          <a>
            <xsl:attribute name="href">
              <xsl:value-of select="TEI/text/body/head/p[3]/s[1]/ref[2]/@target" />
            </xsl:attribute>
            <xsl:value-of select="TEI/text/body/head/p[3]/s[1]/ref[2]/desc" /></a>




          <xsl:value-of select="TEI/text/body/head/p[3]/s[1]/seg[3]" />



          <xsl:value-of select="TEI/text/body/head/p[3]/s[2]" />



          <xsl:value-of select="TEI/text/body/head/p[3]/s[3]/seg[1]" />
          <a>
            <xsl:attribute name="href">
              <xsl:value-of select="TEI/text/body/head/p[3]/s[3]/ref/@target" />
            </xsl:attribute>
            <xsl:value-of select="TEI/text/body/head/p[3]/s[3]/ref/desc" /></a>




          <xsl:value-of select="TEI/text/body/head/p[3]/s[3]/seg[2]" />


        </p>
        <p>
          <xsl:attribute name="class">
            <xsl:value-of select="TEI/text/body/head/p[4]/@style" />
          </xsl:attribute>
          <xsl:value-of select="TEI/text/body/head/p[4]/s[1]" />


          <xsl:value-of select="TEI/text/body/head/p[4]/s[2]" />



          <xsl:value-of select="TEI/text/body/head/p[4]/s[3]/seg[1]" />

          <span>
            <xsl:attribute name="class">
              <xsl:value-of select="TEI/text/body/head/p[4]/s[3]/name/@style" />
            </xsl:attribute>
            <xsl:value-of select="TEI/text/body/head/p[4]/s[3]/name" />
          </span>

          <xsl:value-of select="TEI/text/body/head/p[4]/s[3]/seg[2]" />


        </p>
          <!-- [hr 51.5%] -->
        <hr>
          <xsl:attribute name="class">
            <xsl:value-of select="TEI/text/body/head/figure/milestone/@style" />
          </xsl:attribute>
        </hr>
          <!-- text [p]aragraph(s) or similar division(s) -->
        <p>
          <xsl:attribute name="class">
            <xsl:value-of select="TEI/text/body/head/p[5]/@style" />
          </xsl:attribute>
          <xsl:value-of select="TEI/text/body/head/p[5]/s[1]" />


          <xsl:value-of select="TEI/text/body/head/p[5]/s[2]" />


          <xsl:value-of select="TEI/text/body/head/p[5]/s[3]" />

        </p>
      </div>
        <!-- [hr 61.8%] -->
      <hr>
        <xsl:attribute name="class">
          <xsl:value-of select="TEI/text/body/figure[1]/milestone/@style" />
        </xsl:attribute>
      </hr>
        <!-- HTML5 [article or XHTML div] -->
      <div>
          <!-- articular HTML5 [header or XHTML div] -->
        <div>
            <!-- [h2] heading -->
          <h2>
            <xsl:attribute name="class">
              <xsl:value-of select="TEI/text/body/div1[1]/head/title/@style" />
            </xsl:attribute>
            <xsl:value-of select="TEI/text/body/div1[1]/head/title" />
          </h2>
        </div>
          <!-- [hr 51.5%] -->
        <hr>
          <xsl:attribute name="class">
            <xsl:value-of select="TEI/text/body/div1[1]/figure[1]/milestone/@style" />
          </xsl:attribute>
        </hr>
          <!-- HTML5 (sub-)[article or XHTML div] -->
        <div>
            <!-- sub-articular HTML5 [header or XHTML div] -->
          <div>
              <!-- [h3] heading -->
            <h3>
              <xsl:attribute name="class">
                <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/head/title/@style" />
              </xsl:attribute>
              <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/head/title" />
            </h3>
          </div>
            <!-- 1st HTML5 [section or XHTML div] -->
          <div>
              <!-- [h4] heading -->
            <h4>
              <xsl:attribute name="class">
                <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[1]/title/@style" />
              </xsl:attribute>
              <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[1][1]/title" />
            </h4>
              <!-- 1st HTML5 (sub-)[section or XHTML div] -->
            <div>
                <!-- [h5] heading -->
              <h5>
                <xsl:attribute name="class">
                  <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[1]/div4/title/@style" />
                </xsl:attribute>
                <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[1]/div4/title" />
              </h5>
                <!-- HTML5 (sub-sub-)[section or XHTML div] -->
              <div>
                  <!-- any number of text [p]aragraphs or similar divisions -->
                <p>
                  <xsl:attribute name="class">
                    <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[1]/div4/div5/p[1]/@style" />
                  </xsl:attribute>
                  <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[1]/div4/div5/p[1]/s[1]" />


                  <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[1]/div4/div5/p[1]/s[2]" />


                  <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[1]/div4/div5/p[1]/s[3]" />

                </p>
                <p>
                  <xsl:attribute name="class">
                    <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[1]/div4/div5/p[2]/@style" />
                  </xsl:attribute>
                  <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[1]/div4/div5/p[2]/s[1]" />


                  <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[1]/div4/div5/p[2]/s[2]" />


                  <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[1]/div4/div5/p[2]/s[3]" />

                </p>
                <p>
                  <xsl:attribute name="class">
                    <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[1]/div4/div5/p[3]/@style" />
                  </xsl:attribute>
                  <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[1]/div4/div5/p[3]/s[1]/seg[1]" />

                  <span>
                    <xsl:attribute name="class">
                      <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[1]/div4/div5/p[3]/s[1]/name[1]/@style" />
                    </xsl:attribute>
                    <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[1]/div4/div5/p[3]/s[1]/name[1]" />
                  </span>

                  <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[1]/div4/div5/p[3]/s[1]/seg[2]" />

                  <span>
                    <xsl:attribute name="class">
                      <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[1]/div4/div5/p[3]/s[1]/name[2]/@style" />
                    </xsl:attribute>
                    <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[1]/div4/div5/p[3]/s[1]/name[2]" />
                  </span>

                  <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[1]/div4/div5/p[3]/s[1]/seg[3]" />



                  <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[1]/div4/div5/p[3]/s[2]" />


                  <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[1]/div4/div5/p[3]/s[3]" />

                </p>
              </div>
            </div>
          </div>
            <!-- [hr 41.2%] -->
          <hr>
            <xsl:attribute name="class">
              <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/figure[1]/milestone/@style" />
            </xsl:attribute>
          </hr>
            <!-- 2nd HTML5 [section or XHTML div] -->
          <div>
              <!-- [h4] heading -->
            <h4>
              <xsl:attribute name="class">
                <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[2]/title/@style" />
              </xsl:attribute>
              <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[2]/title" />
            </h4>
              <!-- HTML5 (sub-)[section or XHTML div] -->
            <div>
                <!-- [h5] heading -->
              <h5>
                <xsl:attribute name="class">
                  <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[2]/div4/title/@style" />
                </xsl:attribute>
                <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[2]/div4/title" />
              </h5>
                <!-- text [p]aragraph(s) or similar division(s) -->
              <p>
                <xsl:attribute name="class">
                  <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[2]/div4/p/@style" />
                </xsl:attribute>
                <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[2]/div4/p" />
              </p>
                <!-- 1st HTML5 (sub-sub-)[section or XHTML div] -->
              <div>
                  <!-- [h6] heading -->
                <h6>
                  <xsl:attribute name="class">
                    <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[2]/div4/div5[1]/title/@style" />
                  </xsl:attribute>
                  <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[2]/div4/div5[1]/title" />
                </h6>
                  <!-- text [p]aragraph(s) or similar division(s) -->
                <p>
                  <xsl:attribute name="class">
                    <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[2]/div4/div5[1]/p/@style" />
                  </xsl:attribute>
                  <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[2]/div4/div5[1]/p" />
                </p>
              </div>
                <!-- 2nd HTML5 (sub-sub-)[section or XHTML div] -->
              <div>
                  <!-- [h6] heading -->
                <h6>
                  <xsl:attribute name="class">
                    <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[2]/div4/div5[2]/title/@style" />
                  </xsl:attribute>
                  <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[2]/div4/div5[2]/title" />
                </h6>
                  <!-- text [p]aragraph(s) or similar division(s) -->
                <p>
                  <xsl:attribute name="class">
                    <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[2]/div4/div5[2]/p/@style" />
                  </xsl:attribute>
                  <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[2]/div4/div5[2]/p" />
                </p>
              </div>
            </div>
          </div>
            <!-- [hr 41.2%] -->
          <hr>
            <xsl:attribute name="class">
              <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/figure[2]/milestone/@style" />
            </xsl:attribute>
          </hr>
            <!-- 3rd HTML5 [section or XHTML div] -->
          <div>
              <!-- [h4] heading -->
            <h4>
              <xsl:attribute name="class">
                <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[3]/title/@style" />
              </xsl:attribute>
              <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[3]/title" />
            </h4>
              <!-- 1st HTML5 (sub-)[section or XHTML div] -->
            <div>
                <!-- [h5] heading -->
              <h5>
                <xsl:attribute name="class">
                  <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[3]/div4[1]/title/@style" />
                </xsl:attribute>
                <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[3]/div4[1]/title" />
              </h5>
                <!-- text [p]aragraph(s) or similar division(s) -->
              <p>
                <xsl:attribute name="class">
                  <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[3]/div4[1]/p[1]/@style" />
                </xsl:attribute>
                <span>
                  <xsl:attribute name="class">
                    <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[3]/div4[1]/p[1]/s[1]/foreign/@style" />
                  </xsl:attribute>
                  <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[3]/div4[1]/p[1]/s[1]/foreign" />
                </span>

                <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[3]/div4[1]/p[1]/s[1]/seg" />



                <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[3]/div4[1]/p[1]/s[2]" />


                <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[3]/div4[1]/p[1]/s[3]" />

              </p>
              <p>
                <xsl:attribute name="class">
                  <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[3]/div4[1]/p[2]/@style" />
                </xsl:attribute>
                <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[3]/div4[1]/p[2]/s[1]" />


                <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[3]/div4[1]/p[2]/s[2]" />


                <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[3]/div4[1]/p[2]/s[3]" />

              </p>
              <p>
                <xsl:attribute name="class">
                  <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[3]/div4[1]/p[3]/@style" />
                </xsl:attribute>
                <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[3]/div4[1]/p[3]/s[1]" />


                <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[3]/div4[1]/p[3]/s[2]" />


                <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[3]/div4[1]/p[3]/s[3]" />

              </p>
              <p>
                <xsl:attribute name="class">
                  <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[3]/div4[1]/p[4]/@style" />
                </xsl:attribute>
                <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[3]/div4[1]/p[4]/s[1]" />


                <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[3]/div4[1]/p[4]/s[2]" />

              </p>
              <p>
                <xsl:attribute name="class">
                  <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[3]/div4[1]/p[5]/@style" />
                </xsl:attribute>
                <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[3]/div4[1]/p[5]/s[1]" />


                <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[3]/div4[1]/p[5]/s[2]" />


                <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[3]/div4[1]/p[5]/s[3]" />

              </p>
              <p>
                <xsl:attribute name="class">
                  <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[3]/div4[1]/p[6]/@style" />
                </xsl:attribute>
                <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[3]/div4[1]/p[6]/s[1]" />


                <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[3]/div4[1]/p[6]/s[2]" />


                <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[3]/div4[1]/p[6]/s[3]" />

              </p>
            </div>
              <!-- [hr 30.9%] -->
            <hr>
              <xsl:attribute name="class">
                <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[3]/figure/milestone/@style" />
              </xsl:attribute>
            </hr>
              <!-- 2nd HTML5 (sub-)[section or XHTML div] -->
            <div>
                <!-- [h5] heading -->
              <h5>
                <xsl:attribute name="class">
                  <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[3]/div4[2]/title/@style" />
                </xsl:attribute>
                <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[3]/div4[2]/title" />
              </h5>
                <!-- text [p]aragraph(s) or similar division(s) -->
              <p>
                <xsl:attribute name="class">
                  <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[3]/div4[2]/p[1]/@style" />
                </xsl:attribute>
                <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[3]/div4[2]/p[1]/s[1]" />


                <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[3]/div4[2]/p[1]/s[2]" />


                <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[3]/div4[2]/p[1]/s[3]" />

              </p>
              <p>
                <xsl:attribute name="class">
                  <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[3]/div4[2]/p[2]/@style" />
                </xsl:attribute>
                <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[3]/div4[2]/p[2]/s[1]" />


                <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[3]/div4[2]/p[2]/s[2]" />


                <xsl:value-of select="TEI/text/body/div1[1]/div2[1]/div3[3]/div4[2]/p[2]/s[3]" />

              </p>
            </div>
          </div>
        </div>
          <!--  [hr 51.5%]  -->
        <hr>
          <xsl:attribute name="class">
            <xsl:value-of select="TEI/text/body/div1[1]/figure/milestone/@style" />
          </xsl:attribute>
        </hr>
          <!--  articular appendix = HTML5 [footer or XHTML div]  -->
        <div>
          <!-- [h3] heading -->
          <h3>
            <xsl:attribute name="class">
              <xsl:value-of select="TEI/text/body/div1[1]/div2[2]/title/@style" />
            </xsl:attribute>
            <xsl:value-of select="TEI/text/body/div1[1]/div2[2]/title" />
          </h3>
            <!-- references = HTML5 [section or XHTML div] -->
          <div>
              <!-- [h4] heading -->
            <h4>
              <xsl:value-of select="TEI/text/body/div1[1]/div2[2]/div3/title" />
            </h4>
              <!-- 1st HTML5 (sub-)[section or XHTML div] -->
            <div>
                <!-- [h5] heading -->
              <h5>
                <xsl:value-of select="TEI/text/body/div1[1]/div2[2]/div3/div4[1]/title" />
              </h5>
                <!-- [ol] -->
              <ol>
                  <!-- [li] -->
                <li>






                  <xsl:value-of select="TEI/text/body/div1[1]/div2[2]/div3/div4[1]/listBibl/bibl[1]/respStmt/persName/surname" />


                  <xsl:value-of select="TEI/text/body/div1[1]/div2[2]/div3/div4[1]/listBibl/bibl[1]/respStmt/persName[1]/forename[1]" />






                  <xsl:value-of select="TEI/text/body/div1[1]/div2[2]/div3/div4[1]/listBibl/bibl[1]/respStmt/note[1]" />



                  <xsl:value-of select="TEI/text/body/div1[1]/div2[2]/div3/div4[1]/listBibl/bibl[1]/respStmt/persName[2]/forename[1]" />





                  <xsl:value-of select="TEI/text/body/div1[1]/div2[2]/div3/div4[1]/listBibl/bibl[1]/respStmt/persName[2]/forename[3]" />





                  <xsl:value-of select="TEI/text/body/div1[1]/div2[2]/div3/div4[1]/listBibl/bibl[1]/respStmt/persName[2]/surname" />
                  <xsl:value-of select="TEI/text/body/div1[1]/div2[2]/div3/div4[1]/listBibl/bibl[1]/respStmt/note[1]" />






                  <xsl:value-of select="TEI/text/body/div1[1]/div2[2]/div3/div4[1]/listBibl/bibl[1]/respStmt/persName[3]/forename[1]" />





                  <xsl:value-of select="TEI/text/body/div1[1]/div2[2]/div3/div4[1]/listBibl/bibl[1]/respStmt/persName[3]/surname" />
                  <xsl:value-of select="TEI/text/body/div1[1]/div2[2]/div3/div4[1]/listBibl/bibl[1]/respStmt/note[1]" />



                  <xsl:value-of select="TEI/text/body/div1[1]/div2[2]/div3/div4[1]/listBibl/bibl[1]/respStmt/persName[4]/forename[1]" />





                  <xsl:value-of select="TEI/text/body/div1[1]/div2[2]/div3/div4[1]/listBibl/bibl[1]/respStmt/persName[4]/surname" />



                  <xsl:value-of select="TEI/text/body/div1[1]/div2[2]/div3/div4[1]/listBibl/bibl[1]/respStmt/note[2]" />



                  <xsl:value-of select="TEI/text/body/div1[1]/div2[2]/div3/div4[1]/listBibl/bibl[1]/respStmt/persName[5]/forename[1]" />





                  <xsl:value-of select="TEI/text/body/div1[1]/div2[2]/div3/div4[1]/listBibl/bibl[1]/respStmt/persName[5]/surname" />.




                  <xsl:value-of select="TEI/text/body/div1[1]/div2[2]/div3/div4[1]/listBibl/bibl[1]/date[1]" />.


                  &#8220;<xsl:value-of select="TEI/text/body/div1[1]/div2[2]/div3/div4[1]/listBibl/bibl[1]/title" />.&#8221;


                  <xsl:value-of select="TEI/text/body/div1[1]/div2[2]/div3/div4[1]/listBibl/bibl[1]/date[2]" />.


                  <xsl:value-of select="TEI/text/body/div1[1]/div2[2]/div3/div4[1]/listBibl/bibl[1]/ref" />.

                </li>
                  <!-- [li] -->
                <li>







                  <xsl:value-of select="TEI/text/body/div1[1]/div2[2]/div3/div4[1]/listBibl/bibl[2]/respStmt/orgName/choice/abbr" />.





                  &#8220;<xsl:value-of select="TEI/text/body/div1[1]/div2[2]/div3/div4[1]/listBibl/bibl[2]/title" />.&#8221;


                  <xsl:value-of select="TEI/text/body/div1[1]/div2[2]/div3/div4[1]/listBibl/bibl[2]/ref" />.

                </li>
              </ol>
            </div>
              <!-- [hr 30.9%] -->
            <hr>
              <xsl:attribute name="class">
                <xsl:value-of select="TEI/text/body/div1[1]/div2[2]/div3/figure/milestone/@style" />
              </xsl:attribute>
            </hr>
              <!-- 2nd HTML5 (sub-)[section or XHTML div] -->
            <div>
                <!-- [h5] heading -->
              <h5>
                <xsl:value-of select="TEI/text/body/div1[1]/div2[2]/div3/div4[2]/title" />
              </h5>
                <!-- [ol] -->
              <ol>
                <xsl:attribute name="start">
                  <xsl:value-of select="TEI/text/body/div1[1]/div2[2]/div3/div4[2]/listBibl/bibl/@sortKey" />
                </xsl:attribute>
                  <!-- [li] -->
                <li>

                  &#8220;<xsl:value-of select="TEI/text/body/div1[1]/div2[2]/div3/div4[2]/listBibl/bibl/title" />.&#8221;


                  <xsl:value-of select="TEI/text/body/div1[1]/div2[2]/div3/div4[2]/listBibl/bibl/date[1]" />.


                  <xsl:value-of select="TEI/text/body/div1[1]/div2[2]/div3/div4[2]/listBibl/bibl/date[2]" />.


                  <xsl:value-of select="TEI/text/body/div1[1]/div2[2]/div3/div4[2]/listBibl/bibl/ref" />.

                </li>
              </ol>
            </div>
          </div>
        </div>
      </div>
        <!-- [hr 61.8%] -->
      <hr>
        <xsl:attribute name="class">
          <xsl:value-of select="TEI/text/body/figure/milestone/@style" />
        </xsl:attribute>
      </hr>
        <!-- post-articular HTML5 [footer or XHTML div] -->
      <div>
        <xsl:attribute name="class">
          <xsl:value-of select="TEI/text/body/div1[2]/@style" />
        </xsl:attribute>
          <!-- navigation [div] -->
        <div>
            <!--  [h6] heading  -->
          <h6>
            <xsl:value-of select="TEI/text/body/div1[2]/div2/title" />
          </h6>
            <!-- links [div] with arrows -->
          <div>
            <span>
              <xsl:attribute name="class">
                <xsl:value-of select="TEI/text/body/div1[2]/div2/div3/figure/@style" />
              </xsl:attribute>
              <img>
                  <!-- [alt] attribute -->
                <xsl:attribute name="alt">
                  <xsl:value-of select="TEI/text/body/div1[2]/div2/div3/figure/graphic/desc" />
                </xsl:attribute>
                <xsl:attribute name="class">
                  <xsl:value-of select="TEI/text/body/div1[2]/div2/div3/figure/graphic/@width" />
                </xsl:attribute>
                <xsl:attribute name="src">
                  <xsl:value-of select="TEI/text/body/div1[2]/div2/div3/figure/graphic/@url" />
                </xsl:attribute>
              </img>
            </span>
            &#xa0;&#xa0;&#xa0;
            <span>
              <a>
                <xsl:attribute name="href">
                  <xsl:value-of select="TEI/text/body/div1[2]/div2/div3/seg/ref/@target" />
                </xsl:attribute>
                <xsl:value-of select="TEI/text/body/div1[2]/div2/div3/seg/ref/desc" />


                &#xa0;&#xa0;&#xa0;
                <span>
                  <xsl:attribute name="class">
                    <xsl:value-of select="TEI/text/body/div1[2]/div2/div3/seg/ref/figure/@style" />
                  </xsl:attribute>
                  <img>
                      <!-- [alt] attribute -->
                    <xsl:attribute name="alt">
                      <xsl:value-of select="TEI/text/body/div1[2]/div2/div3/seg/ref/figure/graphic/desc" />
                    </xsl:attribute>
                    <xsl:attribute name="class">
                      <xsl:value-of select="TEI/text/body/div1[2]/div2/div3/seg/ref/figure/graphic/@width" />
                    </xsl:attribute>
                    <xsl:attribute name="src">
                      <xsl:value-of select="TEI/text/body/div1[2]/div2/div3/seg/ref/figure/graphic/@url" />
                    </xsl:attribute>
                  </img>
                </span>
              </a>
            </span>
          </div>
        </div>
        <br />
          <!-- [hr 61.8%] -->
        <hr>
          <xsl:attribute name="class">
            <xsl:value-of select="TEI/text/body/div1[2]/figure/milestone/@style" />
          </xsl:attribute>
        </hr>
        <div>
          <h6>
            <xsl:value-of select="TEI/text/body/div1[2]/div2[2]/title" />
          </h6>
          <div>
            <xsl:attribute name="class">
              <xsl:value-of select="TEI/text/body/div1[2]/div2[2]/ab/@style" />
            </xsl:attribute>
            <span>
              <xsl:value-of select="TEI/text/body/div1[2]/div2[2]/ab/seg[1]" />
            </span>
            <br />
            <span>
              <xsl:value-of select="TEI/text/body/div1[2]/div2[2]/ab/seg[2]" />
            </span>
            <br />
            <span>
              <xsl:value-of select="TEI/text/body/div1[2]/div2[2]/ab/seg[3]" />
            </span>
            <br />
            <span>
              <xsl:value-of select="TEI/text/body/div1[2]/div2[2]/ab/seg[4]" />
            </span>
          </div>
        </div>
        <div>
          <h6>
            <xsl:value-of select="TEI/text/body/div1[2]/div2[3]/title" />
          </h6>
          <div>
            <xsl:attribute name="class">
              <xsl:value-of select="TEI/text/body/div1[2]/div2[3]/ab/@style" />
            </xsl:attribute>
            <span>
              <xsl:value-of select="TEI/text/body/div1[2]/div2[3]/ab/seg[1]" />
            </span>
            <br />
            <span>
              <xsl:value-of select="TEI/text/body/div1[2]/div2[3]/ab/seg[2]" />
            </span>
          </div>
        </div>
        <div>
          <h6>
            <xsl:value-of select="TEI/text/body/div1[2]/div2[4]/title" />
          </h6>
          <div>
            <xsl:attribute name="class">
              <xsl:value-of select="TEI/text/body/div1[2]/div2[4]/ab/@style" />
            </xsl:attribute>
            <span>
              <xsl:value-of select="TEI/text/body/div1[2]/div2[4]/ab/seg[1]" />
            </span>
            <br />
            <span>
              <xsl:value-of select="TEI/text/body/div1[2]/div2[4]/ab/seg[2]" />
            </span>
          </div>
        </div>
      </div>
    </div>
    <br />
      <!-- [hr 72.1%] -->
    <hr>
      <xsl:attribute name="class">
        <xsl:value-of select="TEI/text/figure[2]/milestone/@style" />
      </xsl:attribute>
    </hr>
    <div>
      <div>
        <xsl:attribute name="class">
          <xsl:value-of select="TEI/text/back/div/@style" />
        </xsl:attribute>
        <h6>
          <xsl:value-of select="TEI/text/back/div/title" />
        </h6>
        <div>
          <xsl:attribute name="class">
            <xsl:value-of select="TEI/text/back/div/ab/@style" />
          </xsl:attribute>
          <span>
            <xsl:value-of select="TEI/text/back/div/ab/seg[1]" />
          </span>
          <br />
          <span>
            <xsl:value-of select="TEI/text/back/div/ab/seg[2]" />
          </span>
          <br />
          <br />
          <span>
            <xsl:value-of select="TEI/text/back/div/ab/seg[3]" />
          </span>
        </div>
      </div>
    </div>
  </body>
</html>
