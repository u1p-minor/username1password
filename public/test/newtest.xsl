<xsl:stylesheet version="1.0"
 xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="//titles">
      <xsl:element name="{name}">
      <xsl:value-of select="name"/>
      </xsl:element>
    </xsl:template>

</xsl:stylesheet>
